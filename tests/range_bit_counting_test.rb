require_relative "../src/range_bit_counting"
require 'minitest/autorun'

class RangeBitCountingTest < Minitest::Test
    def test_range_bit_count
        assert range_bit_count(2,7) == 11
        assert range_bit_count(0,1) == 1
        assert range_bit_count(4,4) == 1
    end
end
