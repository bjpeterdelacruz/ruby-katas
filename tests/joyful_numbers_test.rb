require_relative "../src/joyful_numbers"
require 'minitest/autorun'

class JoyfulNumbersTest < Minitest::Test
    def test_number_joy
        assert number_joy(1997) == false
        assert number_joy(1998) == false
        assert number_joy(1729) == true
        assert number_joy(18) == false
        assert number_joy(1) == true
        assert number_joy(81) == true
        assert number_joy(1458) == true
    end
end
