require_relative "../src/reverse_fun"
require 'minitest/autorun'

class ReverseFunTest < Minitest::Test
    def test_reverse_fun
        assert reverse_fun('012') == '201'
        assert reverse_fun('012345') == '504132'
        assert reverse_fun('0123456789') == '9081726354'
        assert reverse_fun('Hello') == 'oHlel'
        assert reverse_fun('4ppso1vdjc9rjyf5bkmd5nztr8') == '84rptpzsno51dvmdkjbc59fryj'
    end
end
