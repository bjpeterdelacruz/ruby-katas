require_relative "../src/odd_even_string_sort"
require 'minitest/autorun'

class OddEvenStringSortTest < Minitest::Test
    def test_sort_my_string
        assert sort_my_string("CodeWars") == "CdWr oeas"

        assert sort_my_string("METHIONYLTHREONYLTHREONYGLUTAMINYLARGINYL") == "MTINLHENLHENGUAIYAGNL EHOYTROYTROYLTMNLRIY"
    end
end
