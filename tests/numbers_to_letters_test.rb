require_relative "../src/numbers_to_letters"
require 'minitest/autorun'

class NumbersToLettersTest < Minitest::Test
    def test_switcher
        assert switcher(['24', '12', '23', '22', '4', '26', '9', '8']) == 'codewars'
        assert switcher(['25','7','8','4','14','23','8','25','23','29','16','16','4']) == 'btswmdsbd kkw'
        assert switcher(['4', '24']) == 'wc'
        assert switcher(['12']) == 'o'
        assert switcher(['12','28','25','21','25','7','11','22','15']) == 'o?bfbtpel'
    end
end
