require_relative "../src/pairs_of_integers"
require 'minitest/autorun'

class PairsOfIntegersTest < Minitest::Test
    def test_generate_pairs
        assert_equal generate_pairs(2), [[0, 0], [0, 1], [0, 2], [1, 1], [1, 2], [2, 2]]
        assert_equal generate_pairs(0), [[0, 0]]
    end
end
