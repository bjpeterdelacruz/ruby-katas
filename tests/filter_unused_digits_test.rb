require_relative "../src/filter_unused_digits"
require 'minitest/autorun'

class FilterUnusedDigitsTest < Minitest::Test
    def test_unused_digits
        assert unused_digits(12, 34, 56, 78) == "09"
        assert unused_digits(2015, 8, 26) == "3479"
        assert unused_digits(276, 575) == "013489"
        assert unused_digits(643) == "0125789"
        assert unused_digits(864, 896, 744) == "01235"
        assert unused_digits(364, 500, 715, 730) == "289"
        assert unused_digits(93, 10, 11, 40) == "25678"
        assert unused_digits() == "0123456789"
    end
end
