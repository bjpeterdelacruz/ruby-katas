require_relative "../src/difference_of_squares"
require 'minitest/autorun'

class DifferenceOfSquaresTest < Minitest::Test
    def test_difference_of_squares
        assert difference_of_squares(5) == 170
        assert difference_of_squares(10) == 2640
        assert difference_of_squares(15) == 13160
        assert difference_of_squares(35) == 381990
        assert difference_of_squares(64) == 4236960
        assert difference_of_squares(100) == 25164150
    end
end
