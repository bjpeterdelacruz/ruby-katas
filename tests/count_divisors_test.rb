require_relative "../src/count_divisors"
require 'minitest/autorun'

class CountDivisorsTest < Minitest::Test
    def test_divisors
        assert divisors(1) == 1
        assert divisors(10) == 4
        assert divisors(11) == 2
        assert divisors(25) == 3
        assert divisors(54) == 8
    end
end
