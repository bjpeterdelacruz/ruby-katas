require_relative "../src/simple_string_characters"
require 'minitest/autorun'

class SimpleStringCharactersTest < Minitest::Test
    def test_solve
        assert_equal solve("Codewars@codewars123.com"), [1, 18, 3, 2]
        assert_equal solve("bgA5<1d-tOwUZTS8yQ"), [7, 6, 3, 2]
        assert_equal solve("P*K4%>mQUDaG$h=cx2?.Czt7!Zn16p@5H"), [9, 9, 6, 9]
        assert_equal solve("RYT'>s&gO-.CM9AKeH?,5317tWGpS<*x2ukXZD"), [15, 8, 6, 9]
        assert_equal solve("$Cnl)Sr<7bBW-&qLHI!mY41ODe"), [10, 7, 3, 6]
        assert_equal solve("@mw>0=QD-iAx!rp9TaG?o&M%l$34L.nbft"), [7, 13, 4, 10]
    end
end
