require_relative "../src/easy_time_convert"
require 'minitest/autorun'

class EasyTimeConvertTest < Minitest::Test
    def test_time_convert
        assert time_convert(0) == '00:00'
        assert time_convert(-6) == '00:00'
        assert time_convert(8) == '00:08'
        assert time_convert(32) == '00:32'
        assert time_convert(75) == '01:15'
        assert time_convert(63) == '01:03'
        assert time_convert(134) == '02:14'
        assert time_convert(180) == '03:00'
        assert time_convert(970) == '16:10'
        assert time_convert(565757) == '9429:17'
    end
end
