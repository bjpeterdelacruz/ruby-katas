require_relative "../src/weird_words"
require 'minitest/autorun'

class WeirdWordsTest < Minitest::Test
    def test_next_letter
        assert next_letter("Hello") == "Ifmmp"
        assert next_letter("What is your name?") == "Xibu jt zpvs obnf?"
        assert next_letter("zOo") == "aPp"
        assert next_letter("ABCXYZ : abcxyz") == "BCDYZA : bcdyza"
    end
end
