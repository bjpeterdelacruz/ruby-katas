require_relative "../src/decoding_message"
require 'minitest/autorun'

class DecodingMessageTest < Minitest::Test
    def test_decode
        assert decode("sr") == "hi"
        assert decode("svool") == "hello"
        assert decode("r slkv mlylwb wvxlwvh gsrh nvhhztv") == "i hope nobody decodes this message"
        assert decode("qzezxirkg rh z srts ovevo wbmznrx fmgbkvw zmw rmgvikivgvw kiltiznnrmt ozmtfztv rg szh yvvm hgzmwziwravw rm gsv vxnzxirkg ozmtfztv hkvxrurxzgrlm zolmthrwv sgno zmw xhh rg rh lmv lu gsv gsivv vhhvmgrzo gvxsmloltrvh lu dliow drwv dvy xlmgvmg kilwfxgrlm gsv nzqlirgb lu dvyhrgvh vnkolb rg zmw rg rh hfkkligvw yb zoo nlwvim dvy yildhvih drgslfg koftrmh") ==  "javacript is a high level dynamic untyped and interpreted programming language it has been standardized in the ecmacript language specification alongside html and css it is one of the three essential technologies of world wide web content production the majority of websites employ it and it is supported by all modern web browsers without plugins"
        assert decode("gsv vrtsgs hbnkslmb dzh qvzm hryvorfh urmzo nzqli xlnklhrgrlmzo kilqvxg lxxfkbrmt srn rmgvinrggvmgob") ==  "the eighth symphony was jean sibelius final major compositional project occupying him intermittently"
        assert decode("husbands ask repeated resolved but laughter debating") ==  "sfhyzmwh zhp ivkvzgvw ivhloevw yfg ozftsgvi wvyzgrmt"
        assert decode("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa") ==  "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
        assert decode(" ") == " "
        assert decode("") == ""
        assert decode("thelastrandomsentence") ==  "gsvozhgizmwlnhvmgvmxv"
    end
end
