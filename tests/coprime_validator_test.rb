require_relative "../src/coprime_validator"
require 'minitest/autorun'

class CoprimeValidatorTest < Minitest::Test
    def test_coprime?
        assert_equal coprime?(20, 27), true
        assert_equal coprime?(12, 39), false
        assert_equal coprime?(17, 34), false
        assert_equal coprime?(34, 17), false
        assert_equal coprime?(35, 10), false
        assert_equal coprime?(64, 27), true
    end
end
