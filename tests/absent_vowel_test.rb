require_relative "../src/absent_vowel"
require 'minitest/autorun'

class AbsentVowelTest < Minitest::Test
    def test_absent_vowel
        assert absent_vowel("John Doe hs seven red pples under his bsket") == 0
        assert absent_vowel("Rain falls arond s and flows nderneath the bridge we are standing on") == 4
    end
end
