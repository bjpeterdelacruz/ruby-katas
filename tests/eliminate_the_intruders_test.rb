require_relative "../src/eliminate_the_intruders"
require 'minitest/autorun'

class EliminateTheIntrudersTest < Minitest::Test
    def test_eliminate_set_bits
        assert eliminate_set_bits("1011") == 7
        assert eliminate_set_bits("111") == 7
        assert eliminate_set_bits("1000000") == 1
        assert eliminate_set_bits("1001") == 3
    end
end
