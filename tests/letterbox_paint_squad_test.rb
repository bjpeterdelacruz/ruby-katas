require_relative "../src/letterbox_paint_squad"
require 'minitest/autorun'

class LetterboxPaintSquadTest < Minitest::Test
    def test_paint_letter_boxes
        assert_equal paint_letter_boxes(120, 123), [1, 5, 5, 1, 0, 0, 0, 0, 0, 0]
    end
end
