require_relative "../src/vowel_one"
require 'minitest/autorun'

class VowelOneTest < Minitest::Test
    def test_vowel_one
        assert vowel_one("vowelOne") == "01010101"
        assert vowel_one("123, arou") == "000001011"
        assert vowel_one("aefgooh") == "1100110"
        assert vowel_one("hello, world") == "010010001000"
        assert vowel_one("aeiou   ") == "11111000"
        assert vowel_one("") == ""
        assert vowel_one("00000") == "00000"
        assert vowel_one("11111") == "00000"
    end
end
