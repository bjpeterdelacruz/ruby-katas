require_relative "../src/reverse_bits"
require 'minitest/autorun'

class ReverseBitsTest < Minitest::Test
    def test_reverse
        assert 417.reverse == 267
        assert 267.reverse == 417
        assert 0.reverse == 0
        assert 2017.reverse == 1087
        assert 1023.reverse == 1023
        assert 1024.reverse == 1
    end
end
