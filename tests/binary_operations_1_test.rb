require_relative "../src/binary_operations_1"
require 'minitest/autorun'

class BinaryOperations1Test < Minitest::Test
    def test_flip_bit
        assert flip_bit(0, 1) == 1
        assert flip_bit(0, 16) == (1 << 15)
        assert flip_bit(4000000, 1) == 4000001
        assert flip_bit((1 << 31) - 1, 31) == ((1 << 30) - 1)
        assert flip_bit(127, 8) == 0xFF
    end
end
