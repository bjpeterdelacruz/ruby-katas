require_relative "../src/filter_even_length_words"
require 'minitest/autorun'

class FilterEvenLengthWordsTest < Minitest::Test
    def test_filter_even_length_words
        assert_equal filter_even_length_words(["Hello", "World"]), []
        assert_equal filter_even_length_words(["One", "Two", "Three", "Four"]), ["Four"]
    end
end
