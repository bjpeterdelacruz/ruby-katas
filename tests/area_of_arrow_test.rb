require_relative "../src/area_of_arrow"
require 'minitest/autorun'

class AreaOfArrowTest < Minitest::Test
    def test_arrow_area
        assert arrow_area(4, 2) == 2
        assert arrow_area(6, 7) == 10.5
        assert arrow_area(10, 10) == 25
        assert arrow_area(25, 25) == 156.25
    end
end
