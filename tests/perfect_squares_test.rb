require_relative "../src/perfect_squares"
require 'minitest/autorun'

class PerfectSquaresTest < Minitest::Test
    def test_get_squares
        assert_equal(get_squares((1..16)), [1,4,9,16])
        assert_equal(get_squares((1..100)), [1,4,9,16,25,36,49,64,81,100])
        assert_equal(get_squares([4,1,16,1,10,35,22]), [1,4,16])

        assert_equal(get_squares((1..4)), [1,4])
        assert_equal(get_squares((25..100)), [25,36,49,64,81,100])
        assert_equal(get_squares((7..20)), [9,16])
        assert_equal(get_squares((10000..10001)), [10000])

        assert_equal(get_squares([4,1]),                [1,4])
        assert_equal(get_squares([36,25,49,81,64,100]), [25,36,49,64,81,100])
        assert_equal(get_squares([16,2,3,16,4]),        [4,16])
        assert_equal(get_squares([4,64,36,22,16]),       [4,16,36,64])

        assert_equal(get_squares([4,4,4,4,1,1,1,1]),          [1,4])
        assert_equal(get_squares([4,4,4,2,2,16,36,2]),        [4,16,36])
        assert_equal(get_squares([9,4,4,4,3,2,2,2,16]),       [4,9,16])
        assert_equal(get_squares([64,64,2,2,2,2,16,4,36,36]), [4,16,36,64])
    end
end
