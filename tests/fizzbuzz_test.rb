require_relative "../src/fizzbuzz"
require 'minitest/autorun'

class FizzBuzzTest < Minitest::Test
    def test_fizzbuzz
        assert fizzbuzz(20) == [5,2,1]
        assert fizzbuzz(2020) == [539, 269, 134]
        assert fizzbuzz(4) == [1,0,0]
        assert fizzbuzz(2) == [0,0,0]
        assert fizzbuzz(30) == [8, 4, 1]
        assert fizzbuzz(300) == [80, 40, 19]
        assert fizzbuzz(14) == [4,2,0]
        assert fizzbuzz(141) == [37,19,9]
        assert fizzbuzz(1415) == [377, 188, 94]
        assert fizzbuzz(91415) == [24377, 12188, 6094]
    end
end
