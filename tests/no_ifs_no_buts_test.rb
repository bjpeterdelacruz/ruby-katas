require_relative "../src/no_ifs_no_buts"
require 'minitest/autorun'

class NoIfsNoButsTest < Minitest::Test
    def test_no_ifs_no_buts
        assert_equal no_ifs_no_buts(3, 4), "3 is smaller than 4"
        assert_equal no_ifs_no_buts(4, 3), "4 is greater than 3"
        assert_equal no_ifs_no_buts(4, 4), "4 is equal to 4"
    end
end
