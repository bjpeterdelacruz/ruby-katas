require_relative "../src/delta_bits"
require 'minitest/autorun'

class DeltaBitsTest < Minitest::Test
    def test_convertBits
        assert convertBits(31,14) == 2
        assert convertBits(7,17) == 3
        assert convertBits(31,0) == 5
        assert convertBits(0,0) == 0
        assert convertBits(127681,127681) == 0
        assert convertBits(312312312, 5645657) == 13
        assert convertBits(43, 2009989843) == 17
    end
end
