require_relative "../src/without_the_letter_e"
require 'minitest/autorun'

class WithoutTheLetterETest < Minitest::Test
    def test_find_e
        assert find_e("actual") == 'There is no "e".'
        assert find_e("") == ""
        assert find_e("English") == "1"
        assert find_e("English exercise") == "4"
        assert find_e(nil) == nil
    end
end
