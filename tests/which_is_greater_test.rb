require_relative "../src/which_is_greater"
require 'minitest/autorun'

class WhichIsGreaterTest < Minitest::Test
    def test_even_or_odd
        assert_equal even_or_odd("12"), "Even is greater than Odd"
        assert_equal even_or_odd("1112"), "Odd is greater than Even"
        assert_equal even_or_odd("112"), "Even and Odd are the same"
    end
end
