require_relative "../src/lucky_number"
require 'minitest/autorun'

class LuckyNumberTest < Minitest::Test
    def test_is_lucky
        assert is_lucky(1892376) == true
        assert is_lucky(189237) == false
        assert is_lucky(18922314324324234423437) == false
        assert is_lucky(189223141324324234423437) == true
        assert is_lucky(1892231413243242344321432142343423437) == true
        assert is_lucky(0) == true
    end
end
