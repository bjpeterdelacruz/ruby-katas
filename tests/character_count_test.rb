require_relative "../src/character_count"
require 'minitest/autorun'

class CharacterCountTest < Minitest::Test
    def test_validate_word
        assert validate_word("abcabc") == true
        assert validate_word("Abcabc") == true
        assert validate_word("AbcabcC") == false
        assert validate_word("AbcCBa") == true
        assert validate_word("pippi") == false
        assert validate_word("?!?!?!") == true
        assert validate_word("abc123") == true
        assert validate_word("abcabcd") == false
        assert validate_word("abc!abc!") == true
        assert validate_word("abc:abc") == false
    end
end
