require_relative "../src/is_even"
require 'minitest/autorun'

class IsEvenTest < Minitest::Test
    def test_is_even
        (0..10).each { |value|
            if value % 2 == 0
                assert is_even(value) == true
            else
                assert is_even(value) == false
            end
        }
    end
end
