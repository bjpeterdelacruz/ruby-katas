require_relative "../src/abundant_numbers"
require 'minitest/autorun'

class AbundantNumbersTest < Minitest::Test
    def test_abundant_number?
        assert abundant_number?(12) == true
        assert abundant_number?(18) == true
        assert abundant_number?(37) == false
        assert abundant_number?(120) == true
        assert abundant_number?(77) == false
        assert abundant_number?(118) == false
        assert abundant_number?(5830) == true
        assert abundant_number?(11410) == true
        assert abundant_number?(14771) == false
        assert abundant_number?(11690) == true
    end
end
