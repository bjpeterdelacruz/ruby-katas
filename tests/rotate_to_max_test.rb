require_relative "../src/rotate_to_max"
require 'minitest/autorun'

class RotateToMaxTest < Minitest::Test
    def test_rotate_to_max
        assert rotate_to_max(123) == 321
        assert rotate_to_max(786) == 876
        assert rotate_to_max('001') == 100
        assert rotate_to_max(999) == 999
        assert rotate_to_max(10543) == 54310
    end
end
