require_relative "../src/ones_complement"
require 'minitest/autorun'

class OnesComplementTest < Minitest::Test
    def test_ones_complement
        assert ones_complement("0") == "1"
        assert ones_complement("1") == "0"
        assert ones_complement("01") == "10"
        assert ones_complement("10") == "01"
        assert ones_complement("1101") == "0010"
        assert ones_complement("10001001") == "01110110"
    end
end
