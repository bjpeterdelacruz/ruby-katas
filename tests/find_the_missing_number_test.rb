require_relative "../src/find_the_missing_number"
require 'minitest/autorun'

class FindMissingNumberTest < Minitest::Test
    def test_missing_no
        assert missing_no((0..100).to_a - [5]) == 5
        assert missing_no((0..100).to_a - [10]) == 10
        assert missing_no(((0..100).to_a - [0]).shuffle) == 0
        assert missing_no(((0..100).to_a - [28]).shuffle) == 28
        assert missing_no(((0..100).to_a - [100]).shuffle) == 100
    end
end
