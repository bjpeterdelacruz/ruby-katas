require_relative "../src/anagram_detection"
require 'minitest/autorun'

class AnagramDetectionTest < Minitest::Test
    def test_is_anagram
        assert is_anagram("Twoo", "WooT") == true
        assert is_anagram("apple", "aple") == false
        assert is_anagram("foefet", "toffee") == true
        assert is_anagram("Buckethead", "DeathCubeK") == true
        assert is_anagram("dumble", "bumble") == false
        assert is_anagram("ound", "round") == false
    end
end
