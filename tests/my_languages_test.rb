require_relative "../src/my_languages"
require 'minitest/autorun'

class MyLanguagesTest < Minitest::Test
    def test_my_languages
        assert my_languages({"Java" => 10, "Ruby" => 80, "Python" => 65}) == ["Ruby", "Python"]
        assert my_languages({"Hindi" => 60, "Greek" => 71, "Dutch" => 93}) == ["Dutch", "Greek", "Hindi"]
        assert my_languages({"C++" => 50, "ASM" => 10, "Haskell" => 20}) == []
    end
end
