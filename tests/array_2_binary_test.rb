require_relative "../src/array_2_binary"
require 'minitest/autorun'

class Array2BinaryTest < Minitest::Test
    def test_arr2bin
        assert arr2bin([1,2]) == "11"
        assert arr2bin([1,2,3,4,5]) == "1111"
        assert arr2bin([1,10,100,1000]) == "10001010111"
        assert arr2bin([1,2,-1,-2]) == "0"
        assert arr2bin([1,2,-1,-2,1]) == "1"

        assert arr2bin([1,'a',2]) == false
        assert arr2bin([1,[],2]) == false
        assert arr2bin(['a',[],2]) == false
        assert arr2bin(['a',[],{}]) == false

        assert arr2bin([]) == "0"
    end
end
