require_relative "../src/palindrome_chain_length"
require 'minitest/autorun'

class PalindromeChainLengthTest < Minitest::Test
    def test_palindrome_chain_length
        assert palindrome_chain_length(87) == 4
        assert palindrome_chain_length(1) == 0
        assert palindrome_chain_length(88) == 0
        assert palindrome_chain_length(89) == 24
        assert palindrome_chain_length(10) == 1
    end
end
