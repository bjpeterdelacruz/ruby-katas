require_relative "../src/evens_times_last"
require 'minitest/autorun'

class EvensTimesLastTest < Minitest::Test
    def test_even_last
        assert even_last([2, 3, 4, 5]) == 30
        assert even_last([]) == 0
        assert even_last([2, 2, 2, 2]) == 8
        assert even_last([1, 3, 3, 1, 10]) == 140
        assert even_last([-11, 3, 3, 1, 10]) == 20
    end
end
