require_relative "../src/distinct_digit_year"
require 'minitest/autorun'

class DistinctDigitYearTest < Minitest::Test
    def test_distinct_digit_year
        assert distinct_digit_year(2013) == 2014
        assert distinct_digit_year(1775) == 1780
    end
end
