require_relative "../src/emotional_sort"
require 'minitest/autorun'

class EmotionalSortTest < Minitest::Test
    def test_sort_emotions
        assert sort_emotions([ ':D', 'T_T', ':D', ':(' ], true) == [ ':D', ':D', ':(', 'T_T' ]
        assert sort_emotions([ 'T_T', ':D', ':(', ':(' ], true) == [ ':D', ':(', ':(', 'T_T' ]
        assert sort_emotions([ ':)', 'T_T', ':)', ':D', ':D' ], true) == [ ':D', ':D', ':)', ':)', 'T_T' ]

        assert sort_emotions([ ':D', 'T_T', ':D', ':(' ], false) == [ 'T_T', ':(', ':D', ':D' ]
        assert sort_emotions([ 'T_T', ':D', ':(', ':(' ], false) == [ 'T_T', ':(', ':(', ':D' ]
        assert sort_emotions([ ':)', 'T_T', ':)', ':D', ':D' ], false) ==  [ 'T_T', ':)', ':)', ':D', ':D' ]

        assert sort_emotions([], false) == []
        assert sort_emotions([], true) == []
    end
end
