require_relative "../src/what_comes_after"
require 'minitest/autorun'

class WhatComesAfterTest < Minitest::Test
    def test_comes_after
        assert comes_after("are you really learning Ruby?",'r') == 'eenu'
        assert comes_after("Pirates say arrrrrrrrr",'r') == 'arrrrrrrr'
        assert comes_after("Free coffee for all office workers!",'f') == 'rfeofi'
        assert comes_after("Every Sunday, she reads newspapers.",'s') == 'uhp'
        assert comes_after("king kUnta is the sickest rap song ever kNown",'k') == 'iUeN'
        assert comes_after("p8tice makes pottery p_r p0rfect!",'p') == 'o'
        assert comes_after("nothing to be found here",'z') == ''
    end
end
