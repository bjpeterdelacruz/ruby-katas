require_relative "../src/javascript_filter_1"
require 'minitest/autorun'

class JavaScriptFilter1Test < Minitest::Test
    def test_search_names
            assert search_names([]) == []

            a = [ [ "foo", "foo@foo.com" ], [ "bar_", "bar@bar.com" ] ]
            b = [ [ "bar_", "bar@bar.com" ] ]
            assert search_names(a) == b
            
            a = [ [ "foo", "foo@foo.com" ], [ "bar_", "bar@bar.com" ],
                ["foobar", "hello@dot.com" ], ["barfoo_", "barfoo@t.com"] ]
            b = [ [ "bar_", "bar@bar.com" ], ["barfoo_", "barfoo@t.com"] ]
            assert search_names(a) == b
            
            a = [ [ "f_o_o", "foo@foo.com" ], [ "bar_", "bar@bar.com" ],
                [ "b_a_r_", "foobar@fb.com" ], [ "_barfoot", "bar_foot@fb.com" ], 
                [ "_barfoot_", "bar_foo@fb.com" ] ]
            b = [ [ "bar_", "bar@bar.com" ], [ "b_a_r_", "foobar@fb.com" ],
                [ "_barfoot_", "bar_foo@fb.com" ] ]
            assert search_names(a) == b
    end
end
