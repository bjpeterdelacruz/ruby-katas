require_relative "../src/filter_the_number"
require 'minitest/autorun'

class FilterTheNumberTest < Minitest::Test
    def test_filter_string
        assert_equal filter_string("a1b2c3"), 123
        assert_equal filter_string("aa 111 bb 2c"), 1112
        assert_equal filter_string("123"), 123
        assert_equal filter_string("aaa"), 0
    end
end
