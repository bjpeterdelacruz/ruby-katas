require_relative "../src/unique_numbers"
require 'minitest/autorun'

class UniqueNumbersTest < Minitest::Test
    def test_unique_numbers
        assert unique_numbers([1,1,2,2]) == [1,2]
        assert unique_numbers([1,2,3,1]) == [1,2,3]
        assert unique_numbers([1,1,2,2,3,1,5,6,6,6]) == [1,2,3,5,6]
    end
end
