require_relative "../src/return_even_characters"
require 'minitest/autorun'

class ReturnEvenCharactersTest < Minitest::Test
    def test_even_chars
        assert even_chars("ab") == ["b"]
        assert even_chars("abcdefghijklm") == ["b", "d", "f", "h", "j", "l"]

        assert even_chars("a") == "invalid string"
        assert even_chars('aiqbuwbjqwbckjdwbwkqbefhglqhfjbwqejbcadn.'\
                          'bcaw.jbhwefjbwqkvbweevkj.bwvwbhvjk.dsvbaj'\
                          'dv.hwuvghwuvfhgw.vjhwncv.wecnaw.ecnvw.kej'\
                          'vhnw.evjkhweqv.kjhwqeev.kjbhdjk.vbaewkjva') == "invalid string"
    end
end
