require_relative "../src/counting_array_elements"
require 'minitest/autorun'

class CountingArrayElementsTest < Minitest::Test
    def test_count
        assert_equal count(["james", "james", "john"]), { "james" => 2, "john" => 1 }
    end
end
