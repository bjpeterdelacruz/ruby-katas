require_relative "../src/multiply_characters"
require 'minitest/autorun'

class MultiplyNumbersTest < Minitest::Test
    def test_spam
        assert spam(1) == "hue"
        assert spam(6) == "huehuehuehuehuehue"
        assert spam(14) == "huehuehuehuehuehuehuehuehuehuehuehuehuehue"
    end
end
