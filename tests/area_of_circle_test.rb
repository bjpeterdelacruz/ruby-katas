require_relative "../src/area_of_circle"
require 'minitest/autorun'

class AreaOfCircleTest < Minitest::Test
    def test_circle_area
        assert_equal circle_area("number"), false
        assert_equal circle_area(-1485.86), false
        assert_equal circle_area(43.2673), 5881.25
        assert_equal circle_area(0.2673), 0.22
        assert_equal circle_area(".2673"), 0.22
        assert_equal circle_area(".2.673"), false
        assert_equal circle_area(0), false
        assert_equal circle_area(2673), 22446456.46
    end
end
