require_relative "../src/next_palindrome_number"
require 'minitest/autorun'

class NextPalindromeNumberTest < Minitest::Test
    def test_next_pal
        assert next_pal(120) == 121
        assert next_pal(1) == 2
        assert next_pal(991) == 999
        assert next_pal(999) == 1001
    end
end
