require_relative "../src/halving_sum"
require 'minitest/autorun'

class HalvingSumTest < Minitest::Test
    def test_halving_sum
        assert halving_sum(25) == 47
        assert halving_sum(127) == 247
        assert halving_sum(38) == 73
        assert halving_sum(1) == 1
        assert halving_sum(320) == 638
        assert halving_sum(13) == 23
        assert halving_sum(15) == 26
        assert halving_sum(47) == 89
        assert halving_sum(101) == 198
        assert halving_sum(257) == 512
    end
end
