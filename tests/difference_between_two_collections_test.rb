require_relative "../src/difference_between_two_collections"
require 'minitest/autorun'

class DifferenceBetweenTwoCollectionsTest < Minitest::Test
    def test_diffence_between_two_collections
        assert_equal diffence_between_two_collections(["a", "b"], ["a", "b"]), []
        assert_equal diffence_between_two_collections(["a", "b"], []), ["a", "b"]
        assert_equal diffence_between_two_collections([], ["a", "b"]), ["a", "b"]
        assert_equal diffence_between_two_collections([], []), []
        assert_equal diffence_between_two_collections(["a", "b"], ["a", "b", "z"]), ["z"]
        assert_equal diffence_between_two_collections(["a", "b", "z", "d", "e", "d"], ["a", "b", "j", "j"]), ["d","e","j","z"]
    end
end
