require_relative "../src/greet_me"
require 'minitest/autorun'

class GreetMeTest < Minitest::Test
    def test_greet
        assert greet("JACK") == "Hello Jack!"
        assert greet("yuuna") == "Hello Yuuna!"
        assert greet("Miyuri") == "Hello Miyuri!"
    end
end
