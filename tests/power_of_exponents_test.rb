require_relative "../src/power_of_exponents"
require 'minitest/autorun'

class PowerOfExponentsTest < Minitest::Test
    def test_power
        assert power(2, 3) == 8
        assert power(10, 0) == 1
        assert power(-5, 3) == -125
        assert power(-4, 2) == 16
        assert power(4, -2) == 0.0625
    end
end
