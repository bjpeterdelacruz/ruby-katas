import os, shutil


dirs = [('src', 'tests')]
for directories in dirs:
    code = [file[:file.index('.rb')] for file in os.listdir(directories[0]) if '.rb' in file]
    tests = [test_file[:test_file.index('_test.rb')] for test_file in os.listdir(directories[1]) if '_test.rb' in test_file]
    files = [file for file in code if file not in tests]
    for file in files:
        with open(f'{directories[1]}/{file}_test.rb', 'w') as f:
            f.writelines([])
