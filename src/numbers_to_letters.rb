def switcher(arr)
    letters = " ?!abcdefghijklmnopqrstuvwxyz".reverse
    arr.reduce("") { |string, number| string += letters[number.to_i - 1] }
end
