def fizzbuzz(n)
    arr = [0, 0, 0]
    (3...n).each { |number|
        if number % 3 == 0 && number % 5 == 0
            arr[2] += 1
        elsif number % 3 == 0
            arr[0] += 1
        elsif number % 5 == 0
            arr[1] += 1
        end
    }
    arr
end
