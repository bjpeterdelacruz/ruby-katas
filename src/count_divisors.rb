require "set"

def divisors(n)
    limit = (n ** 0.5).to_i
    all_divisors = [1, n]
    (2..limit).each { |value|
        if n % value == 0
            all_divisors << value
            all_divisors << n / value
        end
    }
    all_divisors.to_set.length
end
