def arr2bin(arr)
    return false if arr.select { |num| num.class != Integer }.length > 0
    arr.sum.to_s(2)
end
