require "set"

def unique_numbers(numbers_array)
    set = Set[]
    result = []
    numbers_array.each { |num|
        unless set.include? num
            set.add num 
            result << num
        end
    }
    result
end
