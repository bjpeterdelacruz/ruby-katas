def next_pal(val)
    next_num = val + 1
    while next_num.to_s.reverse.to_i != next_num
        next_num += 1
    end
    next_num
end
