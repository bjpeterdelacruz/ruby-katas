def count(array)
    counts = {}
    array.each { |element|
        counts[element] = 0 unless counts[element]
        counts[element] += 1
    }
    counts
end
