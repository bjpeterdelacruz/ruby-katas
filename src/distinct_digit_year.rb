require "set"

def distinct_digit_year(year)
    year += 1
    while year.to_s.split('').to_set.length != 4
        year += 1
    end
    year
end
