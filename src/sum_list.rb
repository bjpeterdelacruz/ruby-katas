def sum_no_duplicates(numbers)
    counts = {}
    numbers.each { |number|
        counts[number] = 0 unless counts[number]
        counts[number] += 1
    }
    sum = 0
    counts.each { |number, count| sum += number if count == 1 }
    sum
end
