def comes_after(str, letter)
    result = ""
    ltr = letter.downcase
    str.downcase.split('').each_with_index { |value, index|
        if value == ltr && index + 1 < str.length && /[a-zA-Z]/.match(str[index + 1])
            result += str[index + 1]
        end
    }
    result
end
