def range_bit_count(a, b)
    (a..b).each.reduce(0) { |sum, number| sum + number.to_s(2).split('').select { |bit| bit == '1' }.length }
end
