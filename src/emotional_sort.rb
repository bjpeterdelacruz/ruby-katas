def sort_emotions(arr, order)
    feelings = {':D' => 0, ':)' => 1, ':|' => 2, ':(' => 3, 'T_T' => 4}
    sorted_arr = arr.sort_by { |emotion| feelings[emotion] }
    return sorted_arr.reverse unless order
    sorted_arr
end
