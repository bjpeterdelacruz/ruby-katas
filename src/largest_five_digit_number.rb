def solution(digits)
    max = 0
    (0...digits.length).each { |idx|
        if idx + 4 < digits.length
            num = digits[idx...idx+5].to_i
            max = num if num > max && num > 9999
        end
    }
    max
end
