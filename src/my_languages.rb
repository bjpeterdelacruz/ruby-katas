def my_languages(results)
    results.select { |language, score| score >= 60 }
        .sort_by { |language, score| -score }
        .map { |language| language[0] }
end
