def vowel_one(string)
    new_string = ""
    string.chars.each { |char|
        if /[aeiouAEIOU]/.match char
            new_string += "1"
        else
            new_string += "0"
        end
    }
    new_string
end
