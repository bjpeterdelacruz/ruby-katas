def ones_complement(binary_number)
    binary_number.chars.map { |char| char == '0' ? '1' : '0' }.join('')
end
