def sum_cubes(n)
    (1..n).reduce(0) { |sum, num| sum + (num ** 3) }
end
