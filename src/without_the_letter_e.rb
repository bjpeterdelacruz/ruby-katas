def find_e(s)
    return "" if s == ""
    return nil if s == nil
    count = s.split('').select { |letter| letter.downcase == 'e' }.count
    return 'There is no "e".' if count == 0
    count.to_s
end
