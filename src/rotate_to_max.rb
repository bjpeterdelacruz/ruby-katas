def rotate_to_max(n)
    n.to_s.chars.map { |char| char.to_i }.sort.reverse.join.to_i
end
