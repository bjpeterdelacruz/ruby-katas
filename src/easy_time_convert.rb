def time_convert(num)
    return "00:00" if num <= 0
    hours = num / 60
    minutes = num % 60
    hours.to_s.rjust(2, "0") + ":" + minutes.to_s.rjust(2, "0")
end
