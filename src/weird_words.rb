def next_letter(s)
    s.chars.map { |char|
        if /[a-zA-Z]/.match char
            case char
            when 'z'
                'a'
            when 'Z'
                'A'
            else
                (char.ord + 1).chr
            end
        else
            char
        end
    }.join
end
