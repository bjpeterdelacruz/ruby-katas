def halving_sum(n)
    denominator = 1
    sum = 0
    while denominator <= n
        sum += (n / denominator)
        denominator *= 2
    end
    sum
end
