def paint_letter_boxes(start, finish)
    counts = {}
    (0..9).each { |number| counts[number] = 0 }
    (start..finish).each { |number|
        number.to_s.chars.each { |character| counts[character.to_i] += 1 }
    }
    all_counts = []
    (0..9).each { |number| all_counts << counts[number] }
    all_counts
end
