def convert(number)
    numbers = []
    (1...number.length).step(2) { |index| numbers << (number[index - 1] + number[index]).to_i.chr }
    numbers.join
end
