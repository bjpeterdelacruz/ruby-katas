def is_lucky(number)
    sum = number.to_s.chars.reduce(0) { |sum, num| sum + num.to_i }
    sum == 0 || sum % 9 == 0
end
