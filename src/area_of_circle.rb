def circle_area r
    if /^\d*\.?\d+$/.match r.to_s
        return (Math::PI * r.to_f * r.to_f).round(2) unless r.to_f <= 0
    end
    false
end
