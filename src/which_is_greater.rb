def even_or_odd(s)
    all_evens = s.chars.select { |value| value.to_i % 2 == 0 }.reduce(0) { |sum, num| sum + num.to_i }
    all_odds = s.chars.select { |value| value.to_i % 2 == 1 }.reduce(0) { |sum, num| sum + num.to_i }
    return "Even is greater than Odd" if all_evens > all_odds
    return "Odd is greater than Even" if all_odds > all_evens
    "Even and Odd are the same"
end
