require "set"

def coprime?(n, m)
    factors_n = ((1..n).select { |value| n % value == 0 }).to_set
    factors_m = ((1..m).select { |value| m % value == 0 }).to_set
    intersect_n_m = factors_n.intersection(factors_m)
    intersect_n_m.length == 1 && intersect_n_m.to_a[0] == 1
end
