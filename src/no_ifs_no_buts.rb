def no_ifs_no_buts(a, b)
    while a > b
        return "#{a} is greater than #{b}"
    end
    return "#{a} is smaller than #{b}" unless a == b
    "#{a} is equal to #{b}"
end
