def power(base, exponent)
    if exponent < 0
        base = 1.0 / base
        exponent *= -1
    end
    result = 1
    exponent.times { |_| result *= base }
    result
end
