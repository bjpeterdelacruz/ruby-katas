def generate_pairs(n)
    arr = []
    (0..n).each { |first| (first..n).each { |second| arr << [first, second] } }
    arr
end
