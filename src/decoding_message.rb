def decode(message)
    alphabet = ' abcdefghijklmnopqrstuvwxyz'
    reverse_alphabet = " #{alphabet.reverse}"

    message.split('').map { |letter| reverse_alphabet[alphabet.index(letter)] }.join('')
end
