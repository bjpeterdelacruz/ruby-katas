require "set"

def missing_no(nums)
    (0..100).to_set.difference(nums.to_set).to_a[0]
end
