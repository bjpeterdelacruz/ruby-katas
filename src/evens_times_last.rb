def even_last(numbers) 
    if numbers.length == 0
        return 0
    end
    sum = 0
    numbers.each_with_index { |value, index| sum += value if index % 2 == 0 }
    sum * numbers[-1]
end
