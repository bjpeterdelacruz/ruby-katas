def sort_my_string(s)
    even = ""
    odd = ""
    s.split('').each_with_index { |char, idx|
        if idx % 2 == 0
            even += char
        else
            odd += char
        end
    }
    "#{even} #{odd}"
end
