def squares(x, n)
    if n <= 0
        return []
    end
    results = [x]
    (1...n).each { |num| results << results[-1] ** 2 }
    results
end
