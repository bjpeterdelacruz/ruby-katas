def is_anagram(test, original)
    test.downcase.chars.sort.join == original.downcase.chars.sort.join
end
