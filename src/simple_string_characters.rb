def solve s
    counts = [0, 0, 0, 0]
    s.chars.each { |character|
        if /[A-Z]/.match character
            counts[0] += 1
        elsif /[a-z]/.match character
            counts[1] += 1
        elsif /[0-9]/.match character
            counts[2] += 1
        else
            counts[3] += 1
        end
    }
    counts
end
