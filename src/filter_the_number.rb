def filter_string(string)
    string.chars.select { |char| /[0-9]/.match(char) }.join.to_i
end
