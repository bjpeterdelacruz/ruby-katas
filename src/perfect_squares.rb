require "set"

def get_squares(array)
    array.select { |number| number == (number ** 0.5).to_i ** 2 }.to_set.to_a.sort
end
