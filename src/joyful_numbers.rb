def number_joy(n)
    sum = n.to_s.split('').reduce(0) { |sum, num| sum + num.to_i }
    n % sum == 0 && sum * sum.to_s.reverse.to_i == n
end
