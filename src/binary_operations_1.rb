def flip_bit(value, bit_index)
    binary = value.to_s(2)
    while binary.length < bit_index
        binary = "0" + binary
    end
    if binary[-bit_index] == "0"
        binary[-bit_index] = "1"
    elsif
        binary[-bit_index] = "0"
    end
    binary.to_i(2)
end
