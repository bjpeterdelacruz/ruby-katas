def unused_digits(*args)
    counts = {}
    (0..9).each { |num| counts[num] = 0 }
    args.each { |arg| arg.to_s.chars.each { |char| counts[char.to_i] += 1 } }
    (0..9).select { |num| counts[num] == 0 }.join
end
