require "set"

def diffence_between_two_collections(a, b)
    set_a = a.to_set
    set_b = b.to_set
    set_a.difference(set_b).union(set_b.difference(set_a)).sort
end
