require "set"

def validate_word(word)
    counts = {}
    word.downcase.split('').each { |letter|
        counts[letter] = 0 unless counts[letter]
        counts[letter] += 1
    }
    counts.values.to_set.size == 1
end
