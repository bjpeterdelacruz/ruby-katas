def even_chars(st)
    if st.length < 2 || st.length > 100
        return "invalid string"
    end
    characters = []
    st.split('').each_with_index { |val, idx|
        if idx % 2 == 1
            characters.push val
        end
    }
    characters
end
