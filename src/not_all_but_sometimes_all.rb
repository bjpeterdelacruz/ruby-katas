def remove(str, what)
    what.each { |letter, max|
        count = 0
        while str.index(letter) != nil && count < max
            str = str.sub(letter, '')
            count += 1
        end
    }
    str
end
