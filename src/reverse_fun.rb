def reverse_fun(string)
    new_string = ""
    while string.length > 0
        string = string.split('').reverse.join
        new_string += string[0]
        string = string[1...string.length]
    end
    new_string
end
