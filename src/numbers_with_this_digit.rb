def numbers_with_digit_inside(x, d)
    arr = [0, 0, 0]
    (1..x).each { |num|
        if num.to_s.include? d.to_s
            arr[0] += 1
            arr[1] += num
            arr[2] = 1 if arr[2] == 0
            arr[2] *= num
        end
    }
    arr
end
