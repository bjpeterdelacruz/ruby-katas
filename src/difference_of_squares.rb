def difference_of_squares(n)
    square_of_sum = (1..n).reduce(0) { |sum, num| sum + num } ** 2
    sum_of_squares = (1..n).reduce(0) { |sum, num| sum + (num ** 2) }
    square_of_sum - sum_of_squares
end
