def absent_vowel(str)
    vowels = {'a' => 0, 'e' => 0, 'i' => 0, 'o' => 0, 'u' => 0}
    positions = {'a' => 0, 'e' => 1, 'i' => 2, 'o' => 3, 'u' => 4}
    str.downcase.split('').each { |chr| vowels[chr] += 1 if vowels[chr] }
    vowels.each { |letter, count|
        return positions[letter] if count == 0
    }
end
