def eliminate_set_bits number
    number.chars.select { |bit| bit == "1" }.join.to_i(2)
end
