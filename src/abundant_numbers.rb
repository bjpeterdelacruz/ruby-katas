require "set"

def abundant_number?(num)
    divisors = Set[1]
    (2..(num ** 0.5)).each { |value|
        if num % value == 0
            divisors << value
            divisors << num / value
        end
    }
    divisors.sum > num
end
