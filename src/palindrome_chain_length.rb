def palindrome_chain_length(n)
    count = 0
    while n.to_s != n.to_s.reverse
        n = n + n.to_s.reverse.to_i
        count += 1
    end
    count
end
