def is_even(n)
    n.to_s(2)[-1] == "0"
end
