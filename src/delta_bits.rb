def convertBits(a, b)
    binary_a = a.to_s(2)
    binary_b = b.to_s(2)
    while binary_a.length < binary_b.length
        binary_a = "0" + binary_a
    end
    while binary_b.length < binary_a.length
        binary_b = "0" + binary_b
    end
    count = 0
    (0...binary_a.length).each { |idx| count += 1 if binary_a[idx] != binary_b[idx] }
    count
end
